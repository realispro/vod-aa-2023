package first.web.ui;

import first.model.Cinema;
import first.model.Movie;
import first.service.CinemaService;
import first.service.MovieService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@Slf4j
@RequiredArgsConstructor
public class CinemaController {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @GetMapping("/cinemas")
    String getCinemas(
            Model model,
            @RequestParam(value = "movieId", required = false) Integer movieId) {
        log.info("about to retrieve cinemas");
        List<Cinema> cinemas;
        String title;
        if (movieId == null) {
            cinemas = cinemaService.getAllCinemas();
            title = "All cinemas";
        } else {
            Movie movie = movieService.getMoviesById(movieId);
            cinemas = cinemaService.getCinemasShowingMovie(movieId);
            title = "Cinemas showing '" + movie.getTitle() + "'";
        }

        model.addAttribute("cinemas", cinemas);
        model.addAttribute("title", title);

        return "cinemasView";

    }
}
