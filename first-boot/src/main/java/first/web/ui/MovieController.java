package first.web.ui;

import first.model.Cinema;
import first.model.Movie;
import first.service.CinemaService;
import first.service.MovieService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@Slf4j
@RequiredArgsConstructor
public class MovieController {

    private final MovieService movieService;
    private final CinemaService cinemaService;

    @GetMapping("/movies")
    String getMovies(Model model,
                     @RequestParam(value = "cinemaId", required = false) Integer cinemaId) {
        log.info("about to retrieve movies");

        List<Movie> movies;
        String title;
        if (cinemaId == null) {
            movies = movieService.findAll();
            title = "All movies";
        } else {
            Cinema cinema = cinemaService.getCinemaById(cinemaId);
            movies = movieService.getAllByCinema(cinemaId);
            title = "Movies in cinema '" + cinema.getName() + "'";
        }

        model.addAttribute("movies", movies);
        model.addAttribute("title", title);
        return "moviesView";
    }
}
