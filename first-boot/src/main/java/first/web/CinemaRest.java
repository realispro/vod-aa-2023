package first.web;

import first.model.Cinema;
import first.service.CinemaService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/webapi")
public class CinemaRest {

    private final CinemaService cinemaService;

    @GetMapping("/cinemas")
    List<CinemaDTO> getCinemas(){
        return cinemaService.getAllCinemas().stream()
                .map(CinemaDTO::from)
                .toList();
    }

    @GetMapping("/cinemas/{cinemaId}")
    ResponseEntity<CinemaDTO> getCinemaById(@PathVariable("cinemaId") int cinemaId){
        Cinema cinema = cinemaService.getCinemaById(cinemaId);
        if(cinema!=null){
            return ResponseEntity.ok(CinemaDTO.from(cinema));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/movies/{movieId}/cinemas")
    List<CinemaDTO> getCinemasShowingMovie(@PathVariable("movieId") int movieId){
        return cinemaService.getCinemasShowingMovie(movieId).stream()
                .map(CinemaDTO::from)
                .toList();
    }

    @Data
    static class CinemaDTO {

        private int id;
        private String name;
        private String logo;

        static CinemaDTO from(Cinema cinema){
            CinemaDTO dto = new CinemaDTO();
            dto.setName(cinema.getName());
            dto.setLogo(cinema.getLogo());
            dto.setId(cinema.getId());
            return dto;
        }
    }
}
