package first.web;

import first.model.Director;
import first.model.Movie;
import first.service.MovieService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;

import jakarta.validation.constraints.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/webapi")
public class MovieRest {

    private final MovieService movieService;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;
    private final MovieValidator movieValidator;

    @InitBinder
    void initBinder(WebDataBinder binder){
        binder.addValidators(movieValidator);
    }

    @GetMapping("/movies")
    List<MovieDTO> getMovies(){
        return movieService.findAll().stream()
                            .map(MovieDTO::from)
                            .toList();
    }

    @GetMapping("/movies/{cinemaId}")
    ResponseEntity<MovieDTO> getMoviesByCinemaId(@PathVariable("cinemaId") int cinemaId){
        Movie movie = movieService.getMoviesById(cinemaId);
        if(movie!=null){
            return ResponseEntity.ok(MovieDTO.from(movie));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/cinemas/{movieId}/movies")
    List<MovieDTO> getCinemasByMovieId(@PathVariable("movieId") int movieId){
        return movieService.getAllByCinema(movieId).stream()
                           .map(MovieRest.MovieDTO::from)
                           .toList();
    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(
            @Validated @RequestBody MovieDTO movieDTO,
            Errors errors,
            HttpServletRequest request){
        log.info("about to add movie {}", movieDTO);

        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        Object credentials = authentication.getPrincipal();
        log.info("security context: authorities {}, credentials {}", authorities, credentials);

        if(errors.hasErrors()){
            Locale locale = localeResolver.resolveLocale(request);
            String message = errors.getAllErrors()
                    .stream()
                    .map(oe -> messageSource.getMessage(oe.getCode(), oe.getArguments(), locale))
                    .collect(() -> new StringBuilder("errors: \n"),
                            (stringBuilder, s) -> stringBuilder.append(s).append(" \n"),
                            StringBuilder::append).toString();

            return ResponseEntity.badRequest().body(message);
        }

        Movie movie = movieService.save(movieDTO.toData());

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(MovieDTO.from(movie));

    }

    @Data
    static class MovieDTO {
        private int id;
        @NotNull
        private String title;
        @NotNull
        private String poster;
        private int directorId;

        static MovieDTO from(Movie movie) {
            MovieDTO dto = new MovieDTO();
            dto.setId(movie.getId());
            dto.setTitle(movie.getTitle());
            dto.setPoster(movie.getPoster());
            dto.setDirectorId(movie.getDirector().getId());
            return dto;
        }

        Movie toData(){
            Movie data = new Movie();
            data.setId(this.id);
            data.setTitle(this.title);
            data.setPoster(this.poster);
            Director director = new Director();
            director.setId(this.directorId);
            data.setDirector(director);
            return data;
        }
    }
}
