package first.web;

import first.model.Movie;
import first.repository.DirectorDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@RequiredArgsConstructor
public class MovieValidator implements Validator {

    private final DirectorDao directorDao;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(MovieRest.MovieDTO.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MovieRest.MovieDTO movie = (MovieRest.MovieDTO) target;
        if(!directorDao.existsById(movie.getDirectorId())){
            errors.rejectValue("directorId", "error.movie.director.missing");
        }
    }
}
