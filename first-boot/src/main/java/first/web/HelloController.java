package first.web;

import first.HelloComponent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
public class HelloController {

    private final HelloComponent helloComponent;

    @GetMapping("/hello")
    String sayHello(){
        log.info("about to say hello");
        return helloComponent.sayHello();
    }
}
