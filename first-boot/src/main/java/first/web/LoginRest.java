package first.web;

import first.security.JwtTokenUtil;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class LoginRest {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil tokenUtil;

    @PostMapping("/login")
    ResponseEntity<String> login(@RequestBody Credentials credentials){
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(credentials.username, credentials.password));

        return ResponseEntity.ok()
                .header(HttpHeaders.AUTHORIZATION, tokenUtil.generateAccessToken(credentials.username))
                .body("ok");
    }

    @Data
    static class Credentials{
        private String username;
        private String password;
    }

}
