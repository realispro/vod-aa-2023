package first;

import first.repository.MovieDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Slf4j
@RequiredArgsConstructor
public class FirstRunner implements CommandLineRunner {

    private final HelloComponent helloComponent;
    private final MovieDao movieDao;

    @Override
    public void run(String... args) throws Exception {
        log.info("runner: {}", helloComponent.sayHello());
        log.info("args: {}", Arrays.toString(args));
        log.info("movies count: {}", movieDao.count());
    }
}
