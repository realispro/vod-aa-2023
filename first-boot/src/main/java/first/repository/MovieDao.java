package first.repository;

import first.model.Cinema;
import first.model.Director;
import first.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;

public interface MovieDao extends JpaRepository<Movie, Integer> {

    List<Movie> findByDirector(Director d);

    @Query("select m from Movie m inner join m.cinemas cinema where cinema.id=:cinemaId")
    List<Movie> findByCinema(@Param("cinemaId") int cinemaId);

    List<Movie> findAllByCinemas(Cinema cinema);

}
