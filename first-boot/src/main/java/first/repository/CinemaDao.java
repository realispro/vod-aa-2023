package first.repository;

import first.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;
import java.util.Optional;

import first.model.Cinema;

public interface CinemaDao extends JpaRepository<Cinema, Integer> {

    @Query("select c from Cinema c inner join c.movies movie where movie.id=:movieId")
    List<Cinema> findByMovie(@Param("movieId") int movieId);

    List<Cinema> findAllByMoviesContains(Movie movie);


    Optional<Cinema> findByName(String name);

    List<Cinema> findAllByNameContaining(String name);

}
