package first.repository;

import first.model.Director;
import org.springframework.data.jpa.repository.JpaRepository;


public interface DirectorDao extends JpaRepository<Director, Integer> {

}
