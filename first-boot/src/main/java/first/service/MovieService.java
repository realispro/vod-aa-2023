package first.service;

import first.model.Cinema;
import first.model.Movie;
import first.repository.MovieDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
@RequiredArgsConstructor
public class MovieService {

    private final MovieDao movieDao;

    public List<Movie> findAll() {
        log.info("About to retrieve all movies");
        return movieDao.findAll();
    }

    public Movie getMoviesById(int cinemaId) {
        return movieDao.findById(cinemaId).orElse(null);
    }

    public List<Movie> getAllByCinema(int movieId) {
        return movieDao.findByCinema(movieId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public Movie save(Movie movie) {
        return movieDao.save(movie);
    }
}
