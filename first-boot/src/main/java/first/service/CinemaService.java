package first.service;

import first.model.Movie;
import first.repository.CinemaDao;
import first.repository.MovieDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import first.model.Cinema;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class CinemaService {

    private final CinemaDao cinemaDao;
    private final MovieDao movieDao;

    public List<Cinema> getAllCinemas(){
        log.info("about to retrieve movies");
        return cinemaDao.findAll();
    }

    public Cinema getCinemaById(int cinemaId){
        log.info("about to retrieve cinema {}", cinemaId);
        return cinemaDao.findById(cinemaId).orElse(null);
    }

    public List<Cinema> getCinemasShowingMovie(int movieId){
        log.info("about to retrieve cinemas showing movie {}", movieId);
        return cinemaDao.findByMovie(movieId);
    }
}
