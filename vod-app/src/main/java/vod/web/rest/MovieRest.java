package vod.web.rest;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import vod.model.Cinema;
import vod.model.Director;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;

import java.util.List;
import java.util.Locale;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/webapi")
public class MovieRest {

    private final MovieService movieService;
    private final CinemaService cinemaService;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;


    @GetMapping("/movies")
    List<MovieDTO> getMovies() {
        log.info("about to retrieve movies");
        List<Movie> movies = movieService.getAllMovies();
        log.info("{} movies found", movies.size());
        return movies.stream()
                      .map(MovieDTO::from)
                      .toList();
    }

    @GetMapping("/movies/{movieId}")
    ResponseEntity<MovieDTO> getCinemaById(@PathVariable("movieId") int movieId) {
        log.info("about to retrieve {}", movieId);
        Movie movie = movieService.getMovieById(movieId);
        if (movie == null) {
            return ResponseEntity.notFound()
                                 .build();
        }

        return ResponseEntity.status(HttpStatus.OK)
                             .body(MovieDTO.from(movie));
    }

    @GetMapping("/cinemas/{cinemaId}/movies")
    ResponseEntity<List<MovieDTO>> getMoviesInCinema(@PathVariable("cinemaId") int cinemaId) {
        log.info("about to retrieve movies in cinema {}", cinemaId);
        Cinema cinema = cinemaService.getCinemaById(cinemaId);
        if (cinema == null) {
            return ResponseEntity.notFound()
                                 .build();
        }

        List<Movie> movies = cinemaService.getMoviesInCinema(cinema);
        return ResponseEntity.ok(movies.stream()
                                        .map(MovieDTO::from)
                                        .toList());
    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(
            @Validated @RequestBody MovieDTO movieDTO,
            Errors errors,
            HttpServletRequest request){
        log.info("about to add movie {}", movieDTO);

        if("boom".equals(movieDTO.getTitle())){
            throw new IllegalArgumentException("boom! movie");
        }

        if(errors.hasErrors()){
            /*String message = errors.getAllErrors().stream()
                    .map(oe->oe.getCode())
                    .reduce("errors: \n", (accu, code)->accu + code + "\n");*/
            Locale locale = localeResolver.resolveLocale(request);
            String message = errors.getAllErrors()
                    .stream()
                    .map(oe -> messageSource.getMessage(oe.getCode(), oe.getArguments(), locale))
                    .collect(() -> new StringBuilder("errors: \n"),
                            (stringBuilder, s) -> stringBuilder.append(s).append(" \n"),
                            StringBuilder::append).toString();
            return ResponseEntity.badRequest().body(message);
        }

        Movie movie = movieService.addMovie(movieDTO.toData());

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(MovieDTO.from(movie));

    }


    @Data
    static class MovieDTO {
        private int id;
        @NotNull
        private String title;
        @NotNull
        private String poster;
        private int directorId;

        static MovieDTO from(Movie movie) {
            MovieDTO dto = new MovieDTO();
            dto.setId(movie.getId());
            dto.setTitle(movie.getTitle());
            dto.setPoster(movie.getPoster());
            dto.setDirectorId(movie.getDirector().getId());
            return dto;
        }

        Movie toData(){
            Movie data = new Movie();
            data.setId(this.id);
            data.setTitle(this.title);
            data.setPoster(this.poster);
            Director director = new Director();
            director.setId(this.directorId);
            data.setDirector(director);
            return data;
        }
    }
}
