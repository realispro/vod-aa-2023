package vod.web.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import vod.model.Director;
import vod.service.MovieService;

@Component
@RequiredArgsConstructor
public class MovieValidator implements Validator {

    private final MovieService movieService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(MovieRest.MovieDTO.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MovieRest.MovieDTO movie = (MovieRest.MovieDTO) target;
        Director director = movieService.getDirectorById(movie.getDirectorId());
        if(director==null){
            errors.rejectValue("directorId", "error.movie.director.missing");
        }
    }
}
