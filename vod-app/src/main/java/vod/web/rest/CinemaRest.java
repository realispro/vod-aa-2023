package vod.web.rest;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/webapi")
public class CinemaRest {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @GetMapping("/cinemas") // /cinemas?name=abc
    List<CinemaDTO> getCinemas(
            @RequestParam(value = "name", required = false) String name,
            @RequestHeader(value = "User-Agent", required = false) String userAgent,
            @CookieValue(value = "Cookie_1", required = false) String cookie1){
        log.info("about to retrieve cinemas");
        log.info("request param name: {}", name);
        log.info("request header User-Agent: {}", userAgent);
        log.info("cookie param cookie_1: {}", cookie1);

        if("boom".equals(name)){
            throw new IllegalArgumentException("boom!");
        }

        List<Cinema> cinemas = cinemaService.getAllCinemas();
        log.info("{} cinemas found", cinemas.size());
        return cinemas.stream()
                .map(CinemaDTO::from)
                .toList();
    }

    @GetMapping("/cinemas/{cinemaId}")
    ResponseEntity<CinemaDTO> getCinemaById(@PathVariable("cinemaId") int cinemaId){
        log.info("about to retrieve cinema {}", cinemaId);
        Cinema cinema = cinemaService.getCinemaById(cinemaId);
        if(cinema!=null) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(CinemaDTO.from(cinema));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/movies/{movieId}/cinemas")
    ResponseEntity<List<CinemaDTO>> getCinemasShowingMovie(@PathVariable("movieId") int movieId){
        log.info("about to retrieve cinemas showing movie {}", movieId);
        Movie movie = movieService.getMovieById(movieId);
        if(movie!=null){
            List<Cinema> cinemas = cinemaService.getCinemasByMovie(movie);
            return ResponseEntity.ok(
                    cinemas.stream()
                            .map(CinemaDTO::from)
                            .toList());
        } else {
            return ResponseEntity.notFound().build();
        }
    }



    @Data
    static class CinemaDTO {

        private int id;
        private String name;
        private String logo;

        static CinemaDTO from(Cinema cinema){
            CinemaDTO dto = new CinemaDTO();
            dto.setName(cinema.getName());
            dto.setLogo(cinema.getLogo());
            dto.setId(cinema.getId());
            return dto;
        }

    }
}
