package vod.web.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class HelloRest {

    @GetMapping("/hello")
    String sayHello(){
        return "Hey Universe!";
    }
}
