package vod.web.ui;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;

import java.util.List;

@Controller
@RequiredArgsConstructor
@Slf4j
public class MoviesController {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @GetMapping("/movies")
    String getMovies(Model model,
                      @RequestParam(value = "cinemaId", required = false) Integer cinemaId) {
        log.info("about to retrieve movies");

        List<Movie> movies;
        String title;
        if (cinemaId == null) {
            movies = movieService.getAllMovies();
            title = "All movies";
        } else {
            Cinema cinema = cinemaService.getCinemaById(cinemaId);
            movies = cinemaService.getMoviesInCinema(cinema);
            title = "Movies in cinema '" + cinema.getName() + "'";
        }

        model.addAttribute("movies", movies);
        model.addAttribute("title", title);
        return "movies";
    }

}
