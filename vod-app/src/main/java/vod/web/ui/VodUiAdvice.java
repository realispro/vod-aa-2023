package vod.web.ui;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice(basePackages = "vod.web.ui")
public class VodUiAdvice {

    @ModelAttribute
    void addTip(Model model){
        model.addAttribute("tip", "Cinema paradise");
    }
}
