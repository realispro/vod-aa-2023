package vod.web;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import java.time.LocalTime;

@Data
@Component
public class TimeBasedInterceptor implements HandlerInterceptor {

    private int opening = 8;
    private int closing = 16;

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) throws Exception {
        LocalTime now = LocalTime.now();
        int currentHour = now.getHour();
        if(currentHour>=opening && currentHour<closing){
            return true;
        } else {
            //response.setStatus(444);
            response.sendRedirect("https://st3.depositphotos.com/1010613/13266/i/450/depositphotos_132666546-stock-photo-closed-tag-inside-window.jpg");
            return false;
        }
    }
}
