package spring.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import spring.Person;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;

@Component
@Aspect
public class TravelAspect {

    @Pointcut("execution(public * spring.impl..*(..))")
    private void allInImpl(){}

    @Pointcut("execution(public * *(spring.Person))")
    private void allAcceptingPerson(){}

    @Pointcut("@annotation(spring.config.ExecutionTime)")
    private void executionTimeAnnotated(){}

    @Before("allInImpl()")
    void logEnteringMethod(JoinPoint jp){
        System.out.println("[ENTERING] " + jp.toString() + " on " + jp.getTarget().getClass().getSimpleName());
    }

    @After("allInImpl()")
    void logExitingMethod(JoinPoint jp){
        System.out.println("[EXITING] " + jp.toString() + " on " + jp.getTarget().getClass().getSimpleName());
    }

    @Around("executionTimeAnnotated()")
    Object logExecutionTime(ProceedingJoinPoint jp) throws Throwable {

        Instant start = Instant.now();
        Object returning = jp.proceed(jp.getArgs());
        Instant end = Instant.now();
        System.out.println("[TIME] " + jp.toString() + " execution took "
        + Duration.between(start, end).toNanos() + " millis");
        return returning;
    }

    @Before("allAcceptingPerson()")
    void ticketValidation(JoinPoint jp){
        Person person = (Person) jp.getArgs()[0];
        if(person.getTicket().getValid().isBefore(LocalDate.now())){
            throw new IllegalArgumentException("invalid ticket!");
        } else {
            System.out.println("[Ticket] validation successful on " + jp.getTarget().getClass().getSimpleName());
        }
    }
}
