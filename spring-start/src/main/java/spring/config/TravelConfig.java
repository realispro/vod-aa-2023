package spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableAspectJAutoProxy
public class TravelConfig {

    @Bean("name")
    String name(){
        return "Business trip";
    }

    @Bean
    List<String> meals(){
        return new ArrayList<>(List.of(
                "hamburger",
                "coca-cola"
        ));
    }
}
