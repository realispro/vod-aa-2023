package spring.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import spring.Accomodation;
import spring.Person;


import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import java.util.List;


@Component
@PropertySource("classpath:/meals.properties")
public class Hotel implements Accomodation {

    @Value("${meals.gratis}")
    private String gratis;

    /*@Autowired
    @Qualifier("meals")*/
    @Resource
    private List<String> meals;

    @PostConstruct
    private void addGratis(){
        meals.add(gratis);
    }

    @Override
    public void host(Person p) {
        System.out.println("person " + p + " is being hosted in hotel. meal: " + meals);
    }

    public void setGratis(String gratis) {
        this.gratis = gratis;
    }

    public void setMeals(List<String> meals) {
        this.meals = meals;
    }
}
