package spring.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import spring.Person;
import spring.Transportation;

import jakarta.annotation.PostConstruct;

@Component("plane")
@Primary
@Qualifier("air")
@Lazy(false)
public class Plane implements Transportation {

    @Override
    public void transport(Person p) {
        System.out.println("Person " + p + " is being transported by plane");
    }

    @PostConstruct
    public void postConstruct(){
        System.out.println("plane: post construct");
    }

}
