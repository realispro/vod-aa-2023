package spring;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class SpringRunner implements CommandLineRunner {

    private final Travel travel;

    public SpringRunner(Travel travel) {
        this.travel = travel;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Spring runner" );
        Person nowak = new Person("Adam", "Nowak", new Ticket(LocalDate.now()));

        travel.travel(nowak);
    }
}
