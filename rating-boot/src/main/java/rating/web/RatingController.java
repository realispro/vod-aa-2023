package rating.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.WebExchangeBindException;
import rating.model.Rating;
import rating.repository.RatingRepository;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class RatingController {

    private final RatingRepository repository;
    private final RatingValidator validator;

    @InitBinder
    void initBinder(WebDataBinder binder) {
        binder.addValidators(validator);
    }

    @GetMapping("/ratings")
    List<Rating> getRatingsByMovie(
            @RequestParam(value = "movieId", required = false) Integer movieId,
            @RequestParam(value = "minRate", required = false) Float minRate) {
        log.info("about to retrieve ratings of a movie {}", movieId);

        return movieId == null
                ? minRate == null
                ? repository.findAll()
                : repository.findAllByRateGreaterThan(minRate)
                : repository.findAllByMovieId(movieId);
    }

    @PostMapping("/ratings")
    Mono<ResponseEntity<Rating>> addRating(
            @Validated @RequestBody Mono<Rating> ratingMono) {

        return ratingMono
                .map(rating -> ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(repository.save(rating)))
                .onErrorResume(WebExchangeBindException.class,
                        ex -> {
                            String message = ex.getAllErrors().stream()
                                    .map(ObjectError::getCode)
                                    .reduce("errors:\n", (accu, code) -> accu + code + "\n");
                            log.info("validation message: {}", message);
                            return Mono.just(ResponseEntity.badRequest().build());
                        });

    }

}
