package rating.web;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import rating.model.Rating;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class RatingValidator implements Validator {

    @Value("${vod.app.url}")
    private String vodAppUrl;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(Rating.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Rating rating = (Rating) target;

        RestTemplate restTemplate = new RestTemplate();
        try {

            ResponseEntity<Movie> response = restTemplate.exchange(
                    vodAppUrl + "/movies/" + rating.getMovieId(),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    Movie.class
            );
            if (response.getStatusCode() == HttpStatus.NOT_FOUND) {
                errors.rejectValue("movieId", "movie.not.found");
            } else {
                log.info("movie id confirmed. Title: {}", response.getBody().getTitle());
            }
        }catch(Exception e){
            errors.rejectValue("movieId", "movie.not.found");
        }

        /*Mono<ResponseEntity<Movie>> mono = WebClient.create(vodAppUrl + "/movies/" + rating.getMovieId())
                .get()
                .retrieve()
                .toEntity(Movie.class);

        ResponseEntity<Movie> response = mono.block();
        if (response.getStatusCode() == HttpStatus.NOT_FOUND) {
            errors.rejectValue("movieId", "movie.not.found");
        } else {
            log.info("movie id confirmed. Title: {}", response.getBody().getTitle());
        }*/
    }

    @Data
    static class Movie{
        private int id;
        private String title;
        private String poster;
        private int directorId;
    }
}
